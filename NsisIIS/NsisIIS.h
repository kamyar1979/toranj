// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the NSISIIS_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// NSISIIS_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#include "pluginapi.h"
#ifdef __cplusplus
extern "C" {
#endif

#define NSISIIS_API __declspec(dllexport)

NSISIIS_API void GetIIsInfo(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void ListWebSites(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void CreateWebSite(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void DeleteWebSite(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void GetWebSite(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void ListVDirs(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void CreateVDir(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void DeleteVDir(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void GetVDir(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void ListAppPools(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void CreateAppPool(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void DeleteAppPool(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void GetAppPool(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void Stop(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void Start(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void Pause(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

NSISIIS_API void Resume(HWND hwndParent, int string_size, 
                                      char *variables, stack_t **stacktop,
                                      extra_parameters *extra);

#ifdef __cplusplus
}
#endif

