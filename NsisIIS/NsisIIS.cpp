// NsisIIS.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "NsisIIS.h"
#include <initguid.h>
#include <objbase.h>
#include <iads.h>
#include <adshlp.h>
#include <iiisext.h>
#include <iisext_i.c>
#include <iiscnfg.h>
#include <atlbase.h>
#include <iostream>
#include <string.h>
#include <atlsafe.h>
#include <iiis.h>

#import "C:\WINDOWS\system32\inetsrv\adsiis.dll" named_guids raw_interfaces_only

const LPWSTR adsPATH = L"IIS://localhost/w3svc/1/Root";

bool bAccessRead;
bool bAccessScript;
bool bAccessBrowsing;

NSISIIS_API void GetIIsInfo(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	HRESULT hr;							//Handle		
	CComVariant *pMajorVersion = new CComVariant();
	CComVariant *pMinorVersion = new CComVariant();
	IADs* iAdsObject = NULL;				//Active Directory Container Interface
	IDispatch *pDisp = NULL;				//IDispatch interface object	
	

	hr = ADsGetObject(L"IIS://localhost/w3svc/Info", IID_IADs, (void **)&iAdsObject);	//Get Object of Active Drectory

	if (!SUCCEEDED(hr)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}
	
	iAdsObject->Get(L"MajorIIsVersionNumber", pMajorVersion);
	iAdsObject->Get(L"MinorIIsVersionNumber", pMinorVersion);
	
	char* ver = new char[10];
	sprintf_s(ver, 10,"%d.%d", pMajorVersion->intVal, pMinorVersion->intVal);
	setuservariable(INST_0, ver);

}

NSISIIS_API void ListWebSites(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	HRESULT hr;							//Handle		
	IDispatch *pDisp = NULL;						//IDispatch interface object
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface
	IUnknown *pUnk=NULL;	
	
	hr = ADsGetObject(L"IIS://localhost/w3svc", IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container
		
	if (!SUCCEEDED(hr)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}

	iContainer->get__NewEnum(&pUnk);
	std::string pSiteList;
	
	IEnumVARIANT *pEnum;
	hr = pUnk->QueryInterface(IID_IEnumVARIANT,(void**)&pEnum);	

	if (!SUCCEEDED(hr)) {
		pushstring("Error:Enumeration failed.");
		return;
	}

	BSTR bstr = NULL;
	BSTR className = NULL;
	VARIANT var;
	IADs *iADs = NULL;
	ULONG lFetch;	

	
	VariantInit(&var);
	hr = pEnum->Next(1, &var, &lFetch);
	bool isFirst = true;
	
	USES_CONVERSION;
	while(hr == S_OK)
	{
		if (lFetch == 1)
		{
			pDisp = V_DISPATCH(&var);
			pDisp->QueryInterface(IID_IADs, (void**)&iADs);				
			iADs->get_Class(&className);
			if(wcscmp(className, L"IIsWebServer") == 0)
			{
				if(!isFirst)
				{
					pSiteList.append(",");				
				}
				isFirst = false;

				iADs->get_Name(&bstr);
				CComVariant *pName = new CComVariant();
				CW2A name(bstr);
				pSiteList.append(name);
					
				hr = iADs->Get(L"ServerComment", pName);					
				if(SUCCEEDED(hr))
				{
					CW2A title(pName->bstrVal);
					pSiteList.append(": ").append(title);
				}
								
				SysFreeString(bstr);
				iADs->Release();
			}
			SysFreeString(className);
		}
		VariantClear(&var);
		pDisp->Release();
		pDisp = NULL;
		hr = pEnum->Next(1, &var, &lFetch);			

	};

	iContainer->Release();
	setuservariable(INST_1, static_cast<const char *>(pSiteList.c_str()));
	pushstring("Listing WebSites Succeeded");
}

NSISIIS_API void CreateWebSite(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	char *siteName = new char[100],*path = new char[100],*bindings = new char[100];
	popstring(siteName);
	popstring(path);
	popstring(bindings);	

	char *poolName = getuservariable(INST_1);
	char *accessFlags = getuservariable(INST_2);
	char *defaultDoc = getuservariable(INST_3);
	char *ipSecurity = getuservariable(INST_4);
	char *sslFlags = getuservariable(INST_5);

	CComBSTR clPath = CComBSTR(path);
	CComBSTR cBindings = CComBSTR(bindings);
	CComBSTR cSiteName = CComBSTR(siteName);

	HRESULT hr;										//Handle
	IDispatch *pDisp = NULL;						//IDispatch interface object
	IDispatch *pDirDisp = NULL;						//IDispatch interface object
	IADs* iAds = NULL;								//Active Directory Interface
	IADs* iDirAds = NULL;							//Active Directory Interface
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface
	IADsContainer* iDirContainer = NULL;			//Active Directory Container Interface
	IISApp3 *pApp = NULL;							//Active Directory IIS Application Interface	
	IISIPSecurity *pIIsIpSecurity;					// IIS IP Security Interface
	CComVariant *pIpSecurity = new CComVariant();
	IUnknown *pUnk;
	BSTR className = NULL;
	
	std::string strIpSecurity (ipSecurity);

	int flags = 0;

	
	hr = ADsGetObject(L"IIS://localhost/w3svc" , IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container
	
	iContainer->get__NewEnum(&pUnk);
	std::string pSiteList;
	
	IEnumVARIANT *pEnum;
	ULONG lFetch;
	hr = pUnk->QueryInterface(IID_IEnumVARIANT,(void**)&pEnum);	
	
	long webSiteCount = 0;
	VARIANT var;
	while(pEnum->Next(1, &var, &lFetch) == S_OK)
	{
		if(lFetch == 1)
		{
			pDisp = V_DISPATCH(&var);
			pDisp->QueryInterface(IID_IADs, (void**)&iAds);				
			iAds->get_Class(&className);
			if(wcscmp(className, L"IIsWebServer") == 0)
				webSiteCount++;
		}			
	}


	if (!SUCCEEDED(hr)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}
	
	BSTR siteIndex;
	wsprintf(siteIndex, L"%d", webSiteCount);	
	
	hr = iContainer->Create(L"IIsWebServer", siteIndex, &pDisp); //Create Website with IContainer
	if (!SUCCEEDED(hr)) {
		pushstring("Error creating website.");
		iContainer->Release();
		return;
	}
	
	
	hr = pDisp->QueryInterface(IID_IADs,(void**) &iAds); //IDispatch Interface object of Active Directory	
	
	iAds->Put(L"ServerComment", CComVariant(cSiteName));
	iAds->SetInfo();
	

	BSTR sPath;
	wsprintf(sPath, L"IIS://localhost/w3svc/%d", webSiteCount);	

	hr = ADsGetObject(sPath , IID_IADsContainer, (void **)&iDirContainer);	//Get Object of Active Drectory Container
	if(FAILED(hr))
	{
		pushstring("Error getting ADSI container");
		iContainer->Release();
		return;
	}

	
	iDirContainer->Create(L"IIsWebVirtualDir",  L"Root", &pDirDisp); //Create V Dir with IContainer
	pDirDisp->QueryInterface(IID_IADs,(void**) &iDirAds); //IDispatch Interface object of Active Directory
	iDirAds->QueryInterface( IID_IISApp3, (void **)&pApp ); //IIS Application Interface object of Active Directory
	if(strlen(poolName) > 0)
	{
		iDirAds->QueryInterface( IID_IISApp3, (void **)&pApp ); //IIS Application Interface object of Active Directory
		pApp->AppCreate3(2, CComVariant(poolName), CComVariant(true));							 //Create IIS V Dir Application
	}
	else
	{
		iDirAds->QueryInterface( IID_IISApp2, (void **)&pApp ); //IIS Application Interface object of Active Directory
		pApp->AppCreate2(2);							 //Create IIS V Dir Application				
	}

	pApp->Put(L"Path", CComVariant(clPath));

	iDirAds->Put(L"AppIsolated",CComVariant(1));
	iDirAds->SetInfo();	
	

	if(strchr(accessFlags,'r') != NULL)
		flags |= MD_ACCESS_READ;
	if(strchr(accessFlags,'w') != NULL)
		flags |= MD_ACCESS_WRITE;
	if(strchr(accessFlags,'s') != NULL)
		flags |= MD_ACCESS_SCRIPT;
	if(strchr(accessFlags,'e') != NULL)
		flags |= MD_ACCESS_EXECUTE;
	if(strchr(accessFlags,'c') != NULL)
		flags |= MD_ACCESS_SOURCE;

	if(strlen(defaultDoc) > 0)
	{
		iAds->Put(L"DefaultDoc",CComVariant(defaultDoc));
		iAds->Put(L"DirBrowseFlag",CComVariant(MD_DIRBROW_SHOW_DATE | MD_DIRBROW_SHOW_TIME | MD_DIRBROW_SHOW_SIZE | MD_DIRBROW_SHOW_EXTENSION | MD_DIRBROW_LOADDEFAULT));
	}

	if (!SUCCEEDED(hr)) {
		pushstring("Failed to set website physical path.");
		iAds->Release();
		pDisp->Release();
		iContainer->Release();
		return;
	}	

	if(strlen(bindings) > 0)
	{
		std::string strBindings (bindings);
		CComSafeArray<VARIANT> *pBindingList = new CComSafeArray<VARIANT>();

		pBindingList->Create();

		int index = 0;
		int newIndex = strBindings.find(',', index);
		while(newIndex != -1)
		{
			pBindingList->Add(CComVariant(strBindings.substr(index, newIndex - index).c_str()));
			index = newIndex;
			newIndex = strBindings.find(',', index + 1);
		}
		pBindingList->Add(CComVariant(strBindings.substr(index+1).c_str()));						


		iAds->Put(L"ServerBindings", CComVariant(pBindingList->m_psa));
		if (!SUCCEEDED(hr)) {
			pushstring("Failed to set website bindings.");
			iAds->Release();
			pDisp->Release();
			iContainer->Release();
			return;
		}
	}

	bool grant;
	grant = (strIpSecurity.find("Grant") != -1);
	
	CComSafeArray<VARIANT> *pIpList = new CComSafeArray<VARIANT>();
	CComSafeArray<VARIANT> *pDomainList = new CComSafeArray<VARIANT>();

	pIpList->Create();	
	pDomainList->Create();

	iAds->Get(L"IPSecurity", pIpSecurity);
	hr = pIpSecurity->pdispVal->QueryInterface(IID_IISIPSecurity, (void **) &pIIsIpSecurity);		
		
	if(SUCCEEDED(hr))
	{
		if(grant)
		{
			pIIsIpSecurity->put_GrantByDefault(VARIANT_TRUE);
			UINT ci = 0;
			UINT oi = 0;
			bool open = false, isIp = true;
			
			while(ci < strIpSecurity.length())
			{
				if(open)
				{
					if(strIpSecurity[ci] == ')')
					{
						open = false;
						if(isIp)
							pIpList->Add(CComVariant(strIpSecurity.substr(oi+1,ci-oi-1).c_str()));						
						else
							pDomainList->Add(CComVariant(strIpSecurity.substr(oi+1,ci-oi-1).c_str()));
					}
					else
					{					
						if(strchr("0123456789., ", strIpSecurity[ci]) == NULL)
							isIp = false;
					}
				}
				else
				{
					if(strIpSecurity[ci] == '(')
					{
						open = true;
						isIp = true;
						oi = ci;
					}
				}
				ci++;
			}
			
			if(pIpList->GetCount() > 0)
				pIIsIpSecurity->put_IPDeny(CComVariant(pIpList->m_psa));		
			if(pDomainList->GetCount() > 0)
				pIIsIpSecurity->put_DomainDeny(CComVariant(pDomainList->m_psa));
			
			iAds->Put(L"IpSecurity", CComVariant(pIIsIpSecurity));		
				
		}
		else
		{
			pIIsIpSecurity->put_GrantByDefault(VARIANT_FALSE);
			UINT ci = 0;
			UINT oi = 0;
			bool open = false, isIp = true;
			while(ci < strIpSecurity.length())
			{
				if(open)
				{
					if(strIpSecurity[ci] == ')')
					{
						open = false;
						if(isIp)
							pIpList->Add(CComVariant(strIpSecurity.substr(oi+1,ci-oi-1).c_str()));						
						else
							pDomainList->Add(CComVariant(strIpSecurity.substr(oi+1,ci-oi-1).c_str()));
					}
					else
					{					
						if(strchr("0123456789., ", strIpSecurity[ci]) == NULL)
							isIp = false;
					}
				}
				else
				{
					if(strIpSecurity[ci] == '(')
					{
						open = true;
						isIp = true;
						oi = ci;
					}
				}
				ci++;
			}						
			if(pIpList->GetCount() > 0)
				pIIsIpSecurity->put_IPGrant(CComVariant(pIpList->m_psa));
			if(pDomainList->GetCount() > 0)
				pIIsIpSecurity->put_DomainGrant(CComVariant(pDomainList->m_psa));
		}
	}
	else
	{
		pushstring("Error querying IP Security interface");
	}	

	if(strlen(accessFlags) > 0)
	{		
		hr &= iAds->Put(L"AccessFlags",CComVariant(flags));
		hr &= iAds->Put(L"DirBrowseFlag",CComVariant(strchr(accessFlags,'b') != NULL));
	}
	else
	{
		hr &= iAds->Put(L"AccessFlags",CComVariant(0x201));
		hr &= iAds->Put(L"DirBrowseFlag",CComVariant(VARIANT_FALSE));
	}		

	if (!SUCCEEDED(hr)) {
		pushstring("Failed to set website access flags.");
		return;
	}

	hr = iAds->SetInfo();										//Save Website Information
	if (SUCCEEDED(hr)) {		
		pushstring("Create Website Success.");
	}
	else {
		pushstring("Error: Create WebSite ");
	}

	pIIsIpSecurity->Release();
	iAds->Release();
	pDisp->Release();
	iContainer->Release();
}

NSISIIS_API void DeleteWebSite(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	char *siteId = new char[100];
	popstring(siteId);

	CComBSTR cSiteId = CComBSTR(siteId);	
	HRESULT handleIPools, hr;						//Handle		
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface

	handleIPools = ADsGetObject(L"IIS://localhost/w3svc", IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container
	
	
	if (!SUCCEEDED(handleIPools)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}

	if(iContainer->Delete(L"IIsWebServer",  cSiteId) < 0)
	{
		if (!SUCCEEDED(hr)) {
			pushstring("Error deleting application pool.");
			iContainer->Release();
			return;
		}
	}
		
	iContainer->Release();
	pushstring("Deleting Website Succeeded.");
}


NSISIIS_API void GetWebSite(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	char *siteName = new char[100];
	popstring(siteName);
	
	CComBSTR cSiteName = CComBSTR(siteName);

	HRESULT hr;										//Handle
	IDispatch *pDisp = NULL;						//Idispatch interface object
	IADs* iAds = NULL;								//Active Directory Interface
	IADs* iDirAds = NULL;							//Active Directory Interface
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface
	IADsContainer* iDirContainer = NULL;				//Active Directory Container Interface
	IISApp3 *pApp = NULL;							//Active Directory IIS Application interface
	IISBaseObject *pSite = NULL;
	IISIPSecurity *pIIsIpSecurity;

	CComVariant *pFlag = new CComVariant();	
	CComVariant *pPhysicalPath = new CComVariant();
	CComVariant *pBindings = new CComVariant();
	CComVariant *pDirBrowse = new CComVariant();
	CComVariant *pDefaultDoc = new CComVariant();
	CComVariant *pIpSecurity = new CComVariant();	

	char rights[10] = "";
	std::string pIpList;

	hr = ADsGetObject(L"IIS://localhost/w3svc",				
		IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container

	hr = iContainer->GetObjectW(L"IIsWebServer",  cSiteName, &pDisp);

	if(FAILED(hr))
	{
		pushstring("Error while getting Active Directory interface to Web Server");		
		iContainer->Release();
		return;
	}

	hr = pDisp->QueryInterface(IID_IADs,(void**) &iAds); //IDispatch Interface object of Active Directory

	if(FAILED(hr))
	{
		pushstring("Error while getting IDispatch interface to Web Server");
		pDisp->Release();
		iContainer->Release();
		return;
	}

	iAds->Get(L"ServerBindings", pBindings);
	std::string bindingList;			
	long count = 0;	

	SafeArrayGetUBound(pBindings->parray, 0, &count);
	count++;
	for(long index = 0; index < count; index++)
	{
		CComVariant *item = new CComVariant();
		SafeArrayGetElement(pBindings->parray, &index, item);
		bindingList.append(_bstr_t(item->bstrVal,FALSE));
		if(index < count - 1)
			bindingList.append(", ");
	}	
	
	setuservariable(INST_1, bindingList.c_str());


	BSTR path = new WCHAR[100];
	wsprintf(path, L"IIS://localhost/w3svc/%s/Root", cSiteName);

	hr = ADsGetObject(path,
		IID_IADs, (void **)&iDirAds);	//Get Object of Active Drectory Container		
	

	if(FAILED(hr))
	{
		pushstring("Error while getting IDispatch interface to Root directory.");
		pDisp->Release();
		iContainer->Release();
		return;
	}

	iDirAds->Get(L"Path", pPhysicalPath);
	setuservariable(INST_0, static_cast<const char *>(_bstr_t(pPhysicalPath->bstrVal,FALSE)));

	hr = iDirAds->Get(L"AccessFlags", pFlag);
	int flags = (pFlag->intVal);
	
	
	if((flags & MD_ACCESS_READ) != 0)
		strcat_s(rights,"r");
	if((flags & MD_ACCESS_WRITE) != 0)
		strcat_s(rights,"w");	
	if((flags & MD_ACCESS_EXECUTE) != 0)
		strcat_s(rights,"e");
	if((flags & MD_ACCESS_SCRIPT) != 0)
		strcat_s(rights,"s");
	if((flags & MD_ACCESS_SOURCE) != 0)
		strcat_s(rights,"c");
	
	
	iAds->Get(L"DirBrowseFlag", pDirBrowse);
	if(pDirBrowse->boolVal == VARIANT_TRUE)
		strcat_s(rights,"b");
	
	setuservariable(INST_2, rights);

	iAds->Get(L"DefaultDoc", pDefaultDoc);	
	setuservariable(INST_3, static_cast<const char *>(_bstr_t(pDefaultDoc->bstrVal,FALSE)));
		
	iAds->Get(L"IPSecurity", pIpSecurity);
	hr = pIpSecurity->pdispVal->QueryInterface(IID_IISIPSecurity, (void **) &pIIsIpSecurity);	

	if(SUCCEEDED(hr))
	{
		VARIANT_BOOL *grant = new VARIANT_BOOL();
		VARIANT *pvIpList = new VARIANT();
		VARIANT *pvDomainList = new VARIANT();

		pIIsIpSecurity->get_GrantByDefault(grant);
		
		if(*grant == VARIANT_TRUE)
		{
			pIpList.append("Grant All");
			hr = pIIsIpSecurity->get_IPDeny(pvIpList);
			hr &= pIIsIpSecurity->get_DomainDeny(pvDomainList);
		}
		else
		{
			pIpList.append("Deny All");
			hr = pIIsIpSecurity->get_IPGrant(pvIpList);
			hr &= pIIsIpSecurity->get_DomainGrant(pvDomainList);
		}
		
		if(SUCCEEDED(hr))
		{
			CComSafeArray<VARIANT> *pIpArray = new CComSafeArray<VARIANT>(pvIpList->parray);
			CComSafeArray<VARIANT> *pDomainArray = new CComSafeArray<VARIANT>(pvDomainList->parray);
			ULONG ipCount = pIpArray->GetCount();
			ULONG domainCount = pDomainArray->GetCount();
			if((ipCount > 0) || (domainCount > 0))
				pIpList.append(" Except ");
			
			for(ULONG l = 0; l < ipCount; l++)
			{
				pIpList.append("(");				
				pIpList.append(static_cast<const char*>(_bstr_t(pIpArray->GetAt(l).bstrVal,FALSE)));
				pIpList.append(") ");
			}
			for(ULONG l = 0; l < domainCount; l++)
			{
				pIpList.append("(");
				pIpList.append(static_cast<const char*>(_bstr_t(pDomainArray->GetAt(l).bstrVal,FALSE)));
				pIpList.append(") ");
			}
			
			setuservariable(INST_4, static_cast<const char *>(pIpList.c_str()));
			pIIsIpSecurity->Release();
		}
		pushstring("Successfully got virtual directory info");
	}
	else
	{
		pushstring("Error getting IP Security Info");		
	}
	iAds->Release();
	pDisp->Release();
	iContainer->Release();

}

NSISIIS_API void ListVDirs(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	HRESULT hr;							//Handle		
	IDispatch *pDisp = NULL;						//IDispatch interface object
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface
	IUnknown *pUnk=NULL;	
	
	hr = ADsGetObject(adsPATH, IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container
		
	if (!SUCCEEDED(hr)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}

	iContainer->get__NewEnum(&pUnk);
	std::string pSiteList;
	
	IEnumVARIANT *pEnum;
	hr = pUnk->QueryInterface(IID_IEnumVARIANT,(void**)&pEnum);

	if (!SUCCEEDED(hr)) {
		pushstring("Error:Enumeration failed.");
		return;
	}

	BSTR bstr = NULL;
	BSTR className = NULL;
	VARIANT var;
	IADs *iADs = NULL;
	ULONG lFetch;

	
	VariantInit(&var);
	hr = pEnum->Next(1, &var, &lFetch);
	bool isFirst = true;
	
	USES_CONVERSION;
	while(hr == S_OK)
	{
		if (lFetch == 1)
		{
			pDisp = V_DISPATCH(&var);
			pDisp->QueryInterface(IID_IADs, (void**)&iADs);				
			iADs->get_Class(&className);
			if(wcscmp(className, L"IIsWebVirtualDir") == 0)
			{
				if(!isFirst)
				{
					pSiteList.append(",");				
				}
				isFirst = false;

				iADs->get_Name(&bstr);
				CW2A name(bstr);
				pSiteList.append(name);
					
				SysFreeString(bstr);
				iADs->Release();
			}
			SysFreeString(className);
		}
		VariantClear(&var);
		pDisp->Release();
		pDisp = NULL;
		hr = pEnum->Next(1, &var, &lFetch);				

	};
	
	iContainer->Release();
	setuservariable(INST_1, static_cast<const char *>(pSiteList.c_str()));
	pushstring("Listing Virtual Directories Succeeded");
}

// This is an example of an exported function.
NSISIIS_API void CreateVDir(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	char *appName = new char[100],*path = new char[100];
	popstring(appName);
	popstring(path);	

	char *poolName = getuservariable(INST_1);
	char *accessFlags = getuservariable(INST_2);	
	char *defaultDoc = getuservariable(INST_3);
	char *ipSecurity = getuservariable(INST_4);
	char *sslFlags = getuservariable(INST_5);

	CComBSTR cAppName = CComBSTR(appName);
	CComBSTR clPath = CComBSTR(path);
	HRESULT handleIDir, hr;							//Handle
	IDispatch *pDisp = NULL;						//IDispatch interface object
	IADs* iAds = NULL;								//Active Directory Interface
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface
	IISApp3 *pApp = NULL;							//Active Directory IIS Application Interface
	IISIPSecurity *pIIsIpSecurity;					// IIS IP Security Interface
	CComVariant *pIpSecurity = new CComVariant();
	
	std::string strIpSecurity (ipSecurity);

	int flags = 0;

	handleIDir = ADsGetObject(adsPATH, IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container
	
	
	if (!SUCCEEDED(handleIDir)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}
	if(iContainer->GetObjectW(L"IIsWebVirtualDir",  cAppName, &pDisp) < 0)
	{
		hr = iContainer->Create(L"IIsWebVirtualDir",  cAppName, &pDisp); //Create V Dir with IContainer
		if (!SUCCEEDED(hr)) {
			pushstring("Error creating virtual directory.");
			iContainer->Release();
			return;
		}
	}
	hr = pDisp->QueryInterface(IID_IADs,(void**) &iAds); //IDispatch Interface object of Active Directory
	
			
	if(strlen(poolName) > 0)
	{
		iAds->QueryInterface( IID_IISApp3, (void **)&pApp ); //IIS Application Interface object of Active Directory
		pApp->AppCreate3(2, CComVariant(poolName), CComVariant(true));							 //Create IIS V Dir Application
	}
	else
	{
		iAds->QueryInterface( IID_IISApp2, (void **)&pApp ); //IIS Application Interface object of Active Directory
		pApp->AppCreate2(2);							 //Create IIS V Dir Application				
	}

	iAds->Put(L"AppIsolated",CComVariant(1));

	if(strchr(accessFlags,'r') != NULL)
		flags |= MD_ACCESS_READ;
	if(strchr(accessFlags,'w') != NULL)
		flags |= MD_ACCESS_WRITE;
	if(strchr(accessFlags,'s') != NULL)
		flags |= MD_ACCESS_SCRIPT;
	if(strchr(accessFlags,'e') != NULL)
		flags |= MD_ACCESS_EXECUTE;
	if(strchr(accessFlags,'c') != NULL)
		flags |= MD_ACCESS_SOURCE;

	if(strlen(defaultDoc) > 0)
	{
		iAds->Put(L"DefaultDoc",CComVariant(defaultDoc));
		iAds->Put(L"DirBrowseFlag",CComVariant(MD_DIRBROW_SHOW_DATE | MD_DIRBROW_SHOW_TIME | MD_DIRBROW_SHOW_SIZE | MD_DIRBROW_SHOW_EXTENSION | MD_DIRBROW_LOADDEFAULT));
	}

	hr = iAds->Put(L"Path",CComVariant(clPath) );				//V Dir Physicial path
	if (!SUCCEEDED(hr)) {
		pushstring("Failed to set virtual directory physical path.");
		iAds->Release();
		pDisp->Release();
		iContainer->Release();
		return;
	}	

	bool grant;
	grant = (strIpSecurity.find("Grant") != -1);
	
	CComSafeArray<VARIANT> *pIpList = new CComSafeArray<VARIANT>();
	CComSafeArray<VARIANT> *pDomainList = new CComSafeArray<VARIANT>();

	pIpList->Create();
	pDomainList->Create();

	iAds->Get(L"IPSecurity", pIpSecurity);
	hr = pIpSecurity->pdispVal->QueryInterface(IID_IISIPSecurity, (void **) &pIIsIpSecurity);		
		
	if(SUCCEEDED(hr))
	{
		if(grant)
		{
			pIIsIpSecurity->put_GrantByDefault(VARIANT_TRUE);
			UINT ci = 0;
			UINT oi = 0;
			bool open = false, isIp = true;
			
			while(ci < strIpSecurity.length())
			{
				if(open)
				{
					if(strIpSecurity[ci] == ')')
					{
						open = false;
						if(isIp)
							pIpList->Add(CComVariant(strIpSecurity.substr(oi+1,ci-oi-1).c_str()));						
						else
							pDomainList->Add(CComVariant(strIpSecurity.substr(oi+1,ci-oi-1).c_str()));
					}
					else
					{					
						if(strchr("0123456789., ", strIpSecurity[ci]) == NULL)
							isIp = false;
					}
				}
				else
				{
					if(strIpSecurity[ci] == '(')
					{
						open = true;
						isIp = true;
						oi = ci;
					}
				}
				ci++;
			}
			
			if(pIpList->GetCount() > 0)
				pIIsIpSecurity->put_IPDeny(CComVariant(pIpList->m_psa));		
			if(pDomainList->GetCount() > 0)
				pIIsIpSecurity->put_DomainDeny(CComVariant(pDomainList->m_psa));
			
			iAds->Put(L"IpSecurity", CComVariant(pIIsIpSecurity));		
				
		}
		else
		{
			pIIsIpSecurity->put_GrantByDefault(VARIANT_FALSE);
			UINT ci = 0;
			UINT oi = 0;
			bool open = false, isIp = true;
			while(ci < strIpSecurity.length())
			{
				if(open)
				{
					if(strIpSecurity[ci] == ')')
					{
						open = false;
						if(isIp)
							pIpList->Add(CComVariant(strIpSecurity.substr(oi+1,ci-oi-1).c_str()));						
						else
							pDomainList->Add(CComVariant(strIpSecurity.substr(oi+1,ci-oi-1).c_str()));
					}
					else
					{					
						if(strchr("0123456789., ", strIpSecurity[ci]) == NULL)
							isIp = false;
					}
				}
				else
				{
					if(strIpSecurity[ci] == '(')
					{
						open = true;
						isIp = true;
						oi = ci;
					}
				}
				ci++;
			}						
			if(pIpList->GetCount() > 0)
				pIIsIpSecurity->put_IPGrant(CComVariant(pIpList->m_psa));
			if(pDomainList->GetCount() > 0)
				pIIsIpSecurity->put_DomainGrant(CComVariant(pDomainList->m_psa));
		}
	}
	else
	{
		pushstring("Error querying IP Security interface");
	}	

	if(strlen(accessFlags) > 0)
	{		
		hr &= iAds->Put(L"AccessFlags",CComVariant(flags));
		hr &= iAds->Put(L"EnableDirBrowsing",CComVariant(strchr(accessFlags,'b') != NULL));
	}
	else
	{
		hr &= iAds->Put(L"AccessFlags",CComVariant(0x201));
		hr &= iAds->Put(L"EnableDirBrowsing",CComVariant(VARIANT_FALSE));
	}

	if (!SUCCEEDED(hr)) {
		pushstring("Failed to set virtual directory access flags.");
		return;
	}

	iAds->Put(L"AppFriendlyName",CComVariant(cAppName));	//V Dir application name

	hr = iAds->SetInfo();										//Save V Dir Application Information
	if (SUCCEEDED(hr)) {		
		pushstring("Create Virtual Dir Success.");
	}
	else {
		pushstring("Error: Create Virtual Dir ");
	}

	pIIsIpSecurity->Release();
	iAds->Release();
	pDisp->Release();
	iContainer->Release();
}

NSISIIS_API void DeleteVDir(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	char *appName = new char[100];
	popstring(appName);	

	CComBSTR cAppName = CComBSTR(appName);
	HRESULT handleIDir, hr;							//Handle
	IDispatch *pDisp = NULL;						//Idispatch interface object
	IADs* iAds = NULL;								//Active Directory Interface
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface

	handleIDir = ADsGetObject(adsPATH,				
		IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container
	
	hr = iContainer->Delete(L"IIsWebVirtualDir",  cAppName); //Delete V Dir with IContainer
	if (SUCCEEDED(hr)) {
		pushstring("Delete Virtulal Directory Success");
	}
	else {
		pushstring("Error:IIsWebVirtualDir Delete Failed.");
	}
	iContainer->Release();

}

NSISIIS_API void GetVDir(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	char *appName = new char[100],*path = new char[100];
	popstring(appName);
	popstring(path);
	

	CComBSTR cAppName = CComBSTR(appName);
	
	HRESULT hr;										//Handle
	IDispatch *pDisp = NULL;						//Idispatch interface object
	IADs* iAds = NULL;								//Active Directory Interface
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface
	IISApp3 *pApp = NULL;							//Active Directory IIS Application interface
	IISIPSecurity *pIIsIpSecurity;

	CComVariant *pFlag = new CComVariant();
	CComVariant *pPoolName = new CComVariant();
	CComVariant *pPhysicalPath = new CComVariant();
	CComVariant *pDirBrowse = new CComVariant();
	CComVariant *pDefaultDoc = new CComVariant();
	CComVariant *pIpSecurity = new CComVariant();	

	char rights[10] = "";
	std::string pIpList;

	hr = ADsGetObject(adsPATH,				
		IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container

	iContainer->GetObjectW(L"IIsWebVirtualDir",  cAppName, &pDisp);

	pDisp->QueryInterface(IID_IADs,(void**) &iAds); //Idispatch Interface object of Active Directory
	hr = iAds->QueryInterface( IID_IISApp3, (void **)&pApp ); //IIS Application Interface object of Active Directory
	if(!SUCCEEDED(hr))
	{
		pushstring("Error while getting Active Directory interface to IIS");
		pDisp->Release();
		iContainer->Release();
		return;
	}		
	
	iAds->Get(L"Path", pPhysicalPath);
	setuservariable(INST_0, static_cast<const char *>(_bstr_t(pPhysicalPath->bstrVal,FALSE)));

	iAds->Get(L"AppPoolId", pPoolName);	
	setuservariable(INST_1, static_cast<const char *>(_bstr_t(pPoolName->bstrVal,FALSE)));

	hr = iAds->Get(L"AccessFlags", pFlag);
	int flags = (pFlag->intVal);
	
	
	if((flags & MD_ACCESS_READ) != 0)
		strcat_s(rights,"r");
	if((flags & MD_ACCESS_WRITE) != 0)
		strcat_s(rights,"w");	
	if((flags & MD_ACCESS_EXECUTE) != 0)
		strcat_s(rights,"e");
	if((flags & MD_ACCESS_SCRIPT) != 0)
		strcat_s(rights,"s");
	if((flags & MD_ACCESS_SOURCE) != 0)
		strcat_s(rights,"c");
	
	
	iAds->Get(L"EnableDirBrowsing", pDirBrowse);
	if(pDirBrowse->boolVal == VARIANT_TRUE)
		strcat_s(rights,"b");
	
	setuservariable(INST_2, rights);

	iAds->Get(L"DefaultDoc", pDefaultDoc);	
	setuservariable(INST_3, static_cast<const char *>(_bstr_t(pDefaultDoc->bstrVal,FALSE)));
		
	iAds->Get(L"IPSecurity", pIpSecurity);
	hr = pIpSecurity->pdispVal->QueryInterface(IID_IISIPSecurity, (void **) &pIIsIpSecurity);	

	if(SUCCEEDED(hr))
	{
		VARIANT_BOOL *grant = new VARIANT_BOOL();
		VARIANT *pvIpList = new VARIANT();
		VARIANT *pvDomainList = new VARIANT();

		pIIsIpSecurity->get_GrantByDefault(grant);
		
		if(*grant == VARIANT_TRUE)
		{
			pIpList.append("Grant All");
			hr = pIIsIpSecurity->get_IPDeny(pvIpList);
			hr &= pIIsIpSecurity->get_DomainDeny(pvDomainList);
		}
		else
		{
			pIpList.append("Deny All");
			hr = pIIsIpSecurity->get_IPGrant(pvIpList);
			hr &= pIIsIpSecurity->get_DomainGrant(pvDomainList);
		}
		
		if(SUCCEEDED(hr))
		{
			CComSafeArray<VARIANT> *pIpArray = new CComSafeArray<VARIANT>(pvIpList->parray);
			CComSafeArray<VARIANT> *pDomainArray = new CComSafeArray<VARIANT>(pvDomainList->parray);
			ULONG ipCount = pIpArray->GetCount();
			ULONG domainCount = pDomainArray->GetCount();
			if((ipCount > 0) || (domainCount > 0))
				pIpList.append(" Except ");
			
			for(ULONG l = 0; l < ipCount; l++)
			{
				pIpList.append("(");				
				pIpList.append(static_cast<const char*>(_bstr_t(pIpArray->GetAt(l).bstrVal,FALSE)));
				pIpList.append(") ");
			}
			for(ULONG l = 0; l < domainCount; l++)
			{
				pIpList.append("(");
				pIpList.append(static_cast<const char*>(_bstr_t(pDomainArray->GetAt(l).bstrVal,FALSE)));
				pIpList.append(") ");
			}
			
			setuservariable(INST_4, static_cast<const char *>(pIpList.c_str()));
			pIIsIpSecurity->Release();
		}
		pushstring("Successfully got virtual directory info");
	}
	else
	{
		pushstring("Error getting IP Security Info");		
	}
	iAds->Release();
	pDisp->Release();
	iContainer->Release();
}

NSISIIS_API void ListAppPools(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	HRESULT hr;							//Handle		
	IDispatch *pDisp = NULL;						//IDispatch interface object
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface
	IUnknown *pUnk=NULL;	

	hr = ADsGetObject(L"IIS://localhost/w3svc/AppPools", IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory

	if (!SUCCEEDED(hr)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}
	
	iContainer->get__NewEnum(&pUnk);
	std::string pSiteList;
	
	IEnumVARIANT *pEnum;
	hr = pUnk->QueryInterface(IID_IEnumVARIANT,(void**)&pEnum);

	if (!SUCCEEDED(hr)) {
		pushstring("Error:Enumeration failed.");
		return;
	}

	BSTR bstr = NULL;
	BSTR className = NULL;
	VARIANT var;
	IADs *iADs = NULL;
	ULONG lFetch;

	
	VariantInit(&var);
	hr = pEnum->Next(1, &var, &lFetch);
	bool isFirst = true;
	
	USES_CONVERSION;
	while(hr == S_OK)
	{
		if (lFetch == 1)
		{
			pDisp = V_DISPATCH(&var);
			pDisp->QueryInterface(IID_IADs, (void**)&iADs);				
			iADs->get_Class(&className);
			if(wcscmp(className, L"IIsApplicationPool") == 0)
			{
				if(!isFirst)
				{
					pSiteList.append(",");				
				}
				isFirst = false;
				iADs->get_Name(&bstr);
				CW2A name(bstr);
				pSiteList.append(name).append(", ");
					
				SysFreeString(bstr);
				iADs->Release();
			}
			SysFreeString(className);
		}
		VariantClear(&var);
		pDisp->Release();
		pDisp = NULL;
		hr = pEnum->Next(1, &var, &lFetch);					

	};
	
	iContainer->Release();	
	setuservariable(INST_1, static_cast<const char *>(pSiteList.c_str()));
	pushstring("Listing Application Pools Succeeded");
}

NSISIIS_API void CreateAppPool(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	char *poolName = new char[100];
	popstring(poolName);

	char *dotNETVersion = getuservariable(INST_1);	
	char *pipeline = getuservariable(INST_2);
	char *enable32Bit = getuservariable(INST_3);

	CComBSTR cPoolName = CComBSTR(poolName);	
	HRESULT handleIPools, hr;						//Handle
	IDispatch *pDisp = NULL;						//IDispatch Interface object
	IADs* iAds = NULL;								//Active Directory Interface
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface
	IISApplicationPool *pAppPool = NULL;			//Active Directory IIS Application Interface	

	handleIPools = ADsGetObject(L"IIS://localhost/w3svc/AppPools", IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container
	
	
	if (!SUCCEEDED(handleIPools)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}

	if(iContainer->GetObjectW(L"IIsApplicationPool",  cPoolName, &pDisp) < 0)
	{
		hr = iContainer->Create(L"IIsApplicationPool",  cPoolName, &pDisp); //Create Application Pool with IContainer
		if (!SUCCEEDED(hr)) {
			pushstring("Error creating application pool.");
			iContainer->Release();
			return;
		}
	}

	pDisp->QueryInterface(IID_IADs,(void**) &iAds); //IDispatch Interface object of Active Directory

	iAds->QueryInterface( IID_IISApplicationPool, (void **)&pAppPool ); //IIS Application Interface object of Active Directory	
	
	int pl;
	if(strcmp(strlwr(pipeline), "integrated") == 0)
		pl = 0;
	else if(strcmp(strlwr(pipeline), "classic") == 0)
		pl = 1;
	
	pAppPool->Put(L"Enable32BitAppOnWin64", CComVariant(strcmp(strlwr(enable32Bit), "true") == 0));	
	pAppPool->Put(L"ManagedRuntimeVersion", CComVariant(dotNETVersion));
	pAppPool->Put(L"ManagedPipelineMode", CComVariant(pl));
	
	pAppPool-> SetInfo();

	iAds->Release();
	pDisp->Release();
	iContainer->Release();
	pushstring("Creating Application Pool Succeeded.");
}

NSISIIS_API void DeleteAppPool(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	char *poolName = new char[100];
	popstring(poolName);

	CComBSTR cPoolName = CComBSTR(poolName);	
	HRESULT handleIPools, hr;						//Handle		
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface

	handleIPools = ADsGetObject(L"IIS://localhost/w3svc/AppPools", IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container
	
	
	if (!SUCCEEDED(handleIPools)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}

	if(iContainer->Delete(L"IIsApplicationPool",  cPoolName) < 0)
	{
		if (!SUCCEEDED(hr)) {
			pushstring("Error deleting application pool.");
			iContainer->Release();
			return;
		}
	}
		
	iContainer->Release();
	pushstring("Deleting Application Pool Succeeded.");
}

NSISIIS_API void GetAppPool(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{
	EXDLL_INIT();

	char *poolName = new char[100];
	popstring(poolName);

	CComVariant *dotNETVersion = new CComVariant();
	CComVariant *pipeline = new CComVariant();
	CComVariant *enable32Bit = new CComVariant();

	CComBSTR cPoolName = CComBSTR(poolName);	
	HRESULT handleIPools, hr;						//Handle
	IDispatch *pDisp = NULL;						//IDispatch Interface object
	IADs* iAds = NULL;								//Active Directory Interface
	IADsContainer* iContainer = NULL;				//Active Directory Container Interface
	IISApplicationPool *pAppPool = NULL;			//Active Directory IIS Application Interface	

	handleIPools = ADsGetObject(L"IIS://localhost/w3svc/AppPools", IID_IADsContainer, (void **)&iContainer);	//Get Object of Active Drectory Container
	
	
	if (!SUCCEEDED(handleIPools)) {
		pushstring("Error:ADsGetObject Getting IDispatch:iContainer");
		return;
	}

	if(iContainer->GetObjectW(L"IIsApplicationPool",  cPoolName, &pDisp) < 0)
	{		
		pushstring("Error getting application pool.");
		iContainer->Release();
		return;		
	}

	hr = pDisp->QueryInterface(IID_IADs,(void**) &iAds); //IDispatch Interface object of Active Directory

	iAds->QueryInterface( IID_IISApplicationPool, (void **)&pAppPool ); //IIS Application Interface object of Active Directory	
		
	pAppPool->Get(L"Enable32BitAppOnWin64", enable32Bit);	
	setuservariable(INST_1, (enable32Bit->boolVal == VARIANT_TRUE) ? "true" : "false");

	pAppPool->Get(L"ManagedRuntimeVersion", dotNETVersion);
	setuservariable(INST_2, static_cast<const char *>(_bstr_t(dotNETVersion->bstrVal,FALSE)));

	char* pipelineModes[2] = {"Integrated", "Classic"};
	pAppPool->Get(L"ManagedPipelineMode", pipeline);
	setuservariable(INST_3, pipelineModes[pipeline->intVal]);

	iAds->Release();
	pDisp->Release();
	iContainer->Release();
	pushstring("Getting Application Pool Succeeded.");
}

NSISIIS_API void Stop(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{

	EXDLL_INIT();

	HRESULT hr;							//Handle		
	IADsServiceOperations* webServer;	

	ADsGetObject(L"IIS://localhost/w3svc/1",
		IID_IADsServiceOperations, (void **)&webServer);	//Get Object of Active Drectory Container

	hr = webServer->Stop();
	if(SUCCEEDED(hr))
	{
		pushstring("IIS stopped successfully.");
		webServer->Release();
	}
	else
	{
		pushstring("Error: Failed to stop IIS.");
	}
}

NSISIIS_API void Start(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{

	EXDLL_INIT();

	HRESULT hr;							//Handle		
	IADsServiceOperations* webServer;	

	ADsGetObject(L"IIS://localhost/w3svc/1",				
		IID_IADsServiceOperations, (void **)&webServer);	//Get Object of Active Drectory Container

	hr = webServer->Start();
	if(SUCCEEDED(hr))
	{
		pushstring("IIS started successfully.");
		webServer->Release();
	}
	else
	{
		pushstring("Error: Failed to start IIS.");
	}
}

NSISIIS_API void Pause(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{

	EXDLL_INIT();

	HRESULT hr;							//Handle		
	IADsServiceOperations* webServer;	

	ADsGetObject(L"IIS://localhost/w3svc/1",				
		IID_IADsServiceOperations, (void **)&webServer);	//Get Object of Active Drectory Container

	hr = webServer->Pause();
	if(SUCCEEDED(hr))
	{
		pushstring("IIS started successfully.");
		webServer->Release();
	}
	else
	{
		pushstring("Error: Failed to start IIS.");
	}
}

NSISIIS_API void Resume(HWND hwndParent, int string_size, 
									  char *variables, stack_t **stacktop,
									  extra_parameters *extra)
{

	EXDLL_INIT();

	HRESULT hr;							//Handle		
	IADsServiceOperations* webServer;	

	ADsGetObject(L"IIS://localhost/w3svc/1",				
		IID_IADsServiceOperations, (void **)&webServer);	//Get Object of Active Drectory Container

	hr = webServer->Continue();
	if(SUCCEEDED(hr))
	{
		pushstring("IIS started successfully.");
		webServer->Release();
	}
	else
	{
		pushstring("Error: Failed to start IIS.");
	}
}


